# pytest grid

## Grid configuration
* Hub
  * Fill username and password for router in [/grid_hub/config.toml](grid_hub/config.toml)
  * Install Java
  * Download Selenium Server and put as /grid_hub/selenium-server.jar
  * Run hub with /grid_hub/run_hub.bat
  * Read hub address
* Node (other machine, same network)
  * Install Chrome and Firefox browsers
  * Download drivers for browsers as /grid_node/chromedriver.exe and /grid_node/geckodriver.exe 
  * Install Java
  * Download Selenium Server and put as /grid_node/selenium-server.jar
  * Fill hub address in events data in [/grid_node/config.toml](grid_node/config.toml)
  * Run node with /grid_node/run_node.bat

## Test run
* Smoke tests
  * `pytest --verbose -n auto -m "smoke and desktop" --browser="chrome" --resolution="1920x1080"`
  * `pytest --verbose -n auto -m "smoke and desktop" --browser="chrome" --resolution="1366x768"`
  * `pytest --verbose -n auto -m "smoke and desktop" --browser="firefox" --resolution="1920x1080"`
  * `pytest --verbose -n auto -m "smoke and mobile" --browser="chrome" --mobile_emulation="Galaxy S5"`
* Sanity tests
  * `pytest --verbose -n auto -m "sanity and desktop" --browser="chrome" --resolution="1920x1080"`
  * `pytest --verbose -n auto -m "sanity and desktop" --browser="chrome" --resolution="1366x768"`
  * `pytest --verbose -n auto -m "sanity and desktop" --browser="firefox" --resolution="1920x1080"`
  * `pytest --verbose -n auto -m "sanity and mobile" --browser="chrome" --mobile_emulation="Galaxy S5"`
* Regression tests
  * Home page
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_home_page.py --browser="chrome" --resolution="1920x1080"`
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_home_page.py --browser="chrome" --resolution="1366x768"`
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_home_page.py --browser="firefox" --resolution="1920x1080"`
    * `pytest --verbose -n auto -m "regression and mobile" tests/test_home_page.py --browser="chrome" --mobile_emulation="Galaxy S5"`
  * Category page
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_category_page.py --browser="chrome" --resolution="1920x1080"`
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_category_page.py --browser="chrome" --resolution="1366x768"`
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_category_page.py --browser="firefox" --resolution="1920x1080"`
    * `pytest --verbose -n auto -m "regression and mobile" tests/test_category_page.py --browser="chrome" --mobile_emulation="Galaxy S5"`
  * Product page
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_product_page.py --browser="chrome" --resolution="1920x1080"`
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_product_page.py --browser="chrome" --resolution="1366x768"`
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_product_page.py --browser="firefox" --resolution="1920x1080"`
    * `pytest --verbose -n auto -m "regression and mobile" tests/test_product_page.py --browser="chrome" --mobile_emulation="Galaxy S5"`
  * Cart page
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_cart_page.py --browser="chrome" --resolution="1920x1080"`
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_cart_page.py --browser="chrome" --resolution="1366x768"`
    * `pytest --verbose -n auto -m "regression and desktop" tests/test_cart_page.py --browser="firefox" --resolution="1920x1080"`
    * `pytest --verbose -n auto -m "regression and mobile" tests/test_cart_page.py --browser="chrome" --mobile_emulation="Galaxy S5"`

import pytest

from selenium import webdriver

from data.data_loader import load_config_data


def pytest_addoption(parser):
    parser.addoption("--browser")
    parser.addoption("--resolution")
    parser.addoption("--mobile_emulation")


@pytest.fixture
def browser(request):
    return request.config.getoption("--browser")


@pytest.fixture
def resolution(request):
    return request.config.getoption("--resolution")


@pytest.fixture
def mobile_emulation(request):
    return request.config.getoption("--mobile_emulation")


@pytest.fixture(autouse=True)
def driver(browser, resolution, mobile_emulation):
    options = None
    match browser:
        case "chrome":
            options = webdriver.ChromeOptions()

        case "firefox":
            options = webdriver.FirefoxOptions()

    options.set_capability("browserName", browser)
    options.set_capability("platformName", "WIN10")
    options.add_argument('--lang=en-US')
    options.add_argument('--headless')

    if mobile_emulation:
        options.add_experimental_option("mobileEmulation", {"deviceName": mobile_emulation})

    config_data = load_config_data()
    username = config_data["router"]["username"]
    password = config_data["router"]["password"]
    driver = webdriver.Remote(command_executor=f"http://{username}:{password}@localhost:4444", options=options)
    driver.implicitly_wait(5)

    if resolution:
        x_position = resolution.rfind("x")
        display_width = int(resolution[:x_position])
        display_height = int(resolution[x_position + 1:])
        driver.set_window_size(display_width, display_height)

    yield driver
    driver.quit()

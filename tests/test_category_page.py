import pytest

from pages.category_page import CategoryPage
from pages.product_page import ProductPage
from pages.cart_page import CartPage
from pages.authentication_page import AuthenticationPage

from data.data_loader import load_test_data


class TestCategoryPage:
    test_data = load_test_data()
    test_data_categories_with_products = [(item["Category number"], item["Product name"], item["Product number"]) for
                                          item in test_data["Categories with products"]]
    test_data_categories_numbers = [(item["Category number"]) for item in test_data["Categories"]]

    @pytest.mark.sanity
    @pytest.mark.desktop
    @pytest.mark.mobile
    @pytest.mark.parametrize("category_number,product,product_number", test_data_categories_with_products[:4])
    def test_redirect_to_product_page(self, driver, category_number, product, product_number):
        page = CategoryPage(driver)
        page.load(category_number).click_product_more(product)
        page = ProductPage(driver)
        assert page.get_url() == page.url.format(product_number)

    @pytest.mark.sanity
    @pytest.mark.desktop
    @pytest.mark.mobile
    @pytest.mark.parametrize("category_number,product,product_number", test_data_categories_with_products[:4])
    def test_add_product_to_cart(self, driver, category_number, product, product_number):
        page = CategoryPage(driver)
        page.load(category_number).add_product_to_cart(product, product_number)
        assert page.is_product_in_cart(product)

    @pytest.mark.sanity
    @pytest.mark.desktop
    @pytest.mark.parametrize("category_number", test_data_categories_numbers[:2])
    def test_redirect_to_cart_page_desktop(self, driver, category_number):
        page = CategoryPage(driver)
        page.load(category_number)
        page.click_cart_link()
        page = CartPage(driver)
        assert page.get_url() == page.url

    @pytest.mark.regression
    @pytest.mark.desktop
    @pytest.mark.mobile
    @pytest.mark.parametrize("category_number", test_data_categories_numbers[:2])
    def test_redirect_to_authentication_page(self, driver, category_number):
        page = CategoryPage(driver)
        page.load(category_number).click_sign_in_link()
        page = AuthenticationPage(driver)
        assert page.url in page.get_url()

    @pytest.mark.regression
    @pytest.mark.desktop
    @pytest.mark.parametrize("category_number", test_data_categories_numbers[:2])
    def test_read_contact_map_marker_desktop(self, driver, category_number):
        page = CategoryPage(driver)
        page.load(category_number)
        assert page.get_contact_map_marker() == page.contact_map_marker

    @pytest.mark.regression
    @pytest.mark.mobile
    @pytest.mark.parametrize("category_number", test_data_categories_numbers[:2])
    def test_read_contact_map_marker_mobile(self, driver, category_number):
        page = CategoryPage(driver)
        page.load(category_number).click_store_information_menu()
        assert page.get_contact_map_marker() == page.contact_map_marker

    @pytest.mark.regression
    @pytest.mark.desktop
    @pytest.mark.parametrize("category_number", test_data_categories_numbers[:2])
    def test_read_contact_phone_number_desktop(self, driver, category_number):
        page = CategoryPage(driver)
        page.load(category_number)
        assert page.get_contact_phone_number() == page.contact_phone_number

    @pytest.mark.regression
    @pytest.mark.mobile
    @pytest.mark.parametrize("category_number", test_data_categories_numbers[:2])
    def test_read_contact_phone_number_mobile(self, driver, category_number):
        page = CategoryPage(driver)
        page.load(category_number).click_store_information_menu()
        assert page.get_contact_phone_number() == page.contact_phone_number

    @pytest.mark.regression
    @pytest.mark.desktop
    @pytest.mark.parametrize("category_number", test_data_categories_numbers[:2])
    def test_read_contact_email_desktop(self, driver, category_number):
        page = CategoryPage(driver)
        page.load(category_number)
        assert page.get_contact_email() == page.contact_email

    @pytest.mark.regression
    @pytest.mark.mobile
    @pytest.mark.parametrize("category_number", test_data_categories_numbers[:2])
    def test_read_contact_email_mobile(self, driver, category_number):
        page = CategoryPage(driver)
        page.load(category_number).click_store_information_menu()
        assert page.get_contact_email() == page.contact_email

import pytest

from pages.home_page import HomePage
from pages.cart_page import CartPage
from pages.authentication_page import AuthenticationPage

from data.data_loader import load_test_data


class TestCartPage:
    test_data = load_test_data()
    test_data_products = [(item["Product name"], item["Product number"]) for item in test_data["Products"]]

    @pytest.mark.sanity
    @pytest.mark.desktop
    @pytest.mark.mobile
    def test_empty_cart(self, driver):
        page = CartPage(driver)
        page.load()
        assert page.is_cart_empty()

    @pytest.mark.sanity
    @pytest.mark.desktop
    @pytest.mark.mobile
    @pytest.mark.parametrize("product,product_number", test_data_products[:2])
    def test_proceed_to_checkout(self, driver, product, product_number):
        page = HomePage(driver)
        page.load().add_product_to_cart(product, product_number)
        page = CartPage(driver)
        page.load().click_proceed_to_checkout_button()
        page = AuthenticationPage(driver)
        assert page.url in page.get_url()

    @pytest.mark.regression
    @pytest.mark.desktop
    @pytest.mark.mobile
    @pytest.mark.parametrize("product,product_number", test_data_products[:2])
    def test_delete_product(self, driver, product, product_number):
        page = HomePage(driver)
        page.load().add_product_to_cart(product, product_number)
        page = CartPage(driver)
        page.load().click_delete_product(product)
        assert page.is_cart_empty()

    @pytest.mark.regression
    @pytest.mark.desktop
    @pytest.mark.mobile
    def test_redirect_to_authentication_page(self, driver):
        page = CartPage(driver)
        page.load().click_sign_in_link()
        page = AuthenticationPage(driver)
        assert page.url in page.get_url()

    @pytest.mark.regression
    @pytest.mark.desktop
    def test_read_contact_map_marker_desktop(self, driver):
        page = CartPage(driver)
        page.load()
        assert page.get_contact_map_marker() == page.contact_map_marker

    @pytest.mark.regression
    @pytest.mark.mobile
    def test_read_contact_map_marker_mobile(self, driver):
        page = CartPage(driver)
        page.load().click_store_information_menu()
        assert page.get_contact_map_marker() == page.contact_map_marker

    @pytest.mark.regression
    @pytest.mark.desktop
    def test_read_contact_phone_number_desktop(self, driver):
        page = CartPage(driver)
        page.load()
        assert page.get_contact_phone_number() == page.contact_phone_number

    @pytest.mark.regression
    @pytest.mark.mobile
    def test_read_contact_phone_number_mobile(self, driver):
        page = CartPage(driver)
        page.load().click_store_information_menu()
        assert page.get_contact_phone_number() == page.contact_phone_number

    @pytest.mark.regression
    @pytest.mark.desktop
    def test_read_contact_email_desktop(self, driver):
        page = CartPage(driver)
        page.load()
        assert page.get_contact_email() == page.contact_email

    @pytest.mark.regression
    @pytest.mark.mobile
    def test_read_contact_email_mobile(self, driver):
        page = CartPage(driver)
        page.load().click_store_information_menu()
        assert page.get_contact_email() == page.contact_email

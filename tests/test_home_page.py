import pytest

from pages.home_page import HomePage
from pages.category_page import CategoryPage
from pages.product_page import ProductPage
from pages.cart_page import CartPage
from pages.authentication_page import AuthenticationPage

from data.data_loader import load_test_data


class TestHomePage:
    test_data = load_test_data()
    test_data_categories = [(item["Category name"], item["Category number"]) for item in test_data["Categories"]]
    test_data_products = [(item["Product name"], item["Product number"]) for item in test_data["Products"]]

    @pytest.mark.smoke
    @pytest.mark.desktop
    @pytest.mark.mobile
    def test_title_of_main_page(self, driver):
        page = HomePage(driver)
        page.load()
        assert page.get_title() == page.title

    @pytest.mark.sanity
    @pytest.mark.desktop
    @pytest.mark.parametrize("category,category_number", test_data_categories)
    def test_redirect_to_category_page_desktop(self, driver, category, category_number):
        page = HomePage(driver)
        page.load().click_category(category)
        page = CategoryPage(driver)
        assert page.get_url() == page.url.format(category_number)

    @pytest.mark.sanity
    @pytest.mark.mobile
    @pytest.mark.parametrize("category,category_number", test_data_categories)
    def test_redirect_to_category_page_mobile(self, driver, category, category_number):
        page = HomePage(driver)
        page.load().click_categories_menu().click_category(category)
        page = CategoryPage(driver)
        assert page.get_url() == page.url.format(category_number)

    @pytest.mark.sanity
    @pytest.mark.desktop
    @pytest.mark.mobile
    @pytest.mark.parametrize("product,product_number", test_data_products)
    def test_redirect_to_product_page(self, driver, product, product_number):
        page = HomePage(driver)
        page.load().click_product_more(product)
        page = ProductPage(driver)
        assert page.get_url() == page.url.format(product_number)

    @pytest.mark.sanity
    @pytest.mark.desktop
    @pytest.mark.mobile
    @pytest.mark.parametrize("product,product_number", test_data_products)
    def test_add_product_to_cart(self, driver, product, product_number):
        page = HomePage(driver)
        page.load().add_product_to_cart(product, product_number)
        assert page.is_product_in_cart(product)

    @pytest.mark.sanity
    @pytest.mark.desktop
    def test_redirect_to_cart_page_desktop(self, driver):
        page = HomePage(driver)
        page.load().click_cart_link()
        page = CartPage(driver)
        assert page.get_url() == page.url

    @pytest.mark.regression
    @pytest.mark.desktop
    @pytest.mark.mobile
    def test_redirect_to_authentication_page(self, driver):
        page = HomePage(driver)
        page.load().click_sign_in_link()
        page = AuthenticationPage(driver)
        assert page.url in page.get_url()

    @pytest.mark.regression
    @pytest.mark.desktop
    def test_read_contact_map_marker_desktop(self, driver):
        page = HomePage(driver)
        page.load()
        assert page.get_contact_map_marker() == page.contact_map_marker

    @pytest.mark.regression
    @pytest.mark.mobile
    def test_read_contact_map_marker_mobile(self, driver):
        page = HomePage(driver)
        page.load().click_store_information_menu()
        assert page.get_contact_map_marker() == page.contact_map_marker

    @pytest.mark.regression
    @pytest.mark.desktop
    def test_read_contact_phone_number_desktop(self, driver):
        page = HomePage(driver)
        page.load()
        assert page.get_contact_phone_number() == page.contact_phone_number

    @pytest.mark.regression
    @pytest.mark.mobile
    def test_read_contact_phone_number_mobile(self, driver):
        page = HomePage(driver)
        page.load().click_store_information_menu()
        assert page.get_contact_phone_number() == page.contact_phone_number

    @pytest.mark.regression
    @pytest.mark.desktop
    def test_read_contact_email_desktop(self, driver):
        page = HomePage(driver)
        page.load()
        assert page.get_contact_email() == page.contact_email

    @pytest.mark.regression
    @pytest.mark.mobile
    def test_read_contact_email_mobile(self, driver):
        page = HomePage(driver)
        page.load().click_store_information_menu()
        assert page.get_contact_email() == page.contact_email

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class ProductPage(BasePage):
    def __init__(self, driver):
        BasePage.__init__(self, driver)

        self.url = self.base_url + "/index.php?id_product={}&controller=product"
        self.add_to_cart_button_selector = "p#add_to_cart button"

    def load(self, number):
        self.driver.get(self.url.format(number))
        return self

    def add_product_to_cart(self, *args):
        element = self.driver.find_element(By.CSS_SELECTOR, self.add_to_cart_button_selector)
        ActionChains(self.driver).move_to_element(element).perform()
        element.click()
        return self
